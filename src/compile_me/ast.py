# -*- coding: utf8 -*-
from compile_me.equality import Equality

__author__ = 'cjaym'

last_id = 0

def generate_id():
    """
    Инкрементируем id
    :return:
    """
    global last_id
    last_id += 1
    return last_id


class BaseNode(Equality):
    """
    Базовая нода для построения AST-парсера
    """

    NodeName = "BaseNode"

    def __init__(self, pos):
        self.pos = pos
        self.id = generate_id()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            dict1 = self.__dict__.copy()
            dict2 = other.__dict__.copy()
            if "id" in dict1:
                del dict1["id"]
            if "id" in dict2:
                del dict2["id"]
            if dict1 == dict2:
                return True

        return False

    def get_name(self):
        return "BaseNode"

    def get_name_size(self):
        return 0

    def __iter__(self):
        yield None


    def to_text(self):
        return "|BASE_NODE|"

    def __repr__(self):
        return '|%d| %(node_name)s' % (self.pos, self.NodeName)

    def _stringify_node(self, node):
        if isinstance(node, BaseNode):
            return node.to_text()
        if isinstance(node, str):
            return node
        if isinstance(node, list):
            res = []
            for i in node:
                res.append(self._stringify_node(i))
            return " ".join(res)


class Node(BaseNode):
    """
    Нода, имеющая аттрибует name
    """
    state = 0
    NodeName = "Node"

    def __init__(self, name, pos):
        super(Node, self).__init__(pos)
        self.name = name

    def get_name(self):
        return "Node"

    def __repr__(self):
        return '|%d| %s (%s)' % (self.pos, self.NodeName, self.name)
        # return '%(node_name)s (%s)' % (1, "self.NodeName", "self.name")