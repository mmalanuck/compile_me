# -*- coding: utf8 -*-
__author__ = 'cjay'


combinator_id = 0

def generate_id():
    global combinator_id
    combinator_id += 1
    return combinator_id

class Result(object):
    '''
    Результат, возвращаемый каждым парсером в случае успеха
    '''

    def __init__(self, value, pos, text_pos, index_offset=0):
        self.value = value
        self.pos = pos
        self.text_pos = text_pos+index_offset

    def __repr__(self):
        return 'Result(%s)' % (str(self.value))

class BaseParser(object):
    '''
    Основной класс парсера
    '''

    def __init__(self):
        self.id = generate_id()

    def __add__(self, other):
        return Concat(self, other)

    def __mul__(self, other):
        return Exp(self, other)

    def __or__(self, other):
        return Alternate(self, other)

    def __xor__(self, function):
        return Process(self, function)

class Tag(BaseParser):
    """
    Парсер, который ищет токены с указанным тэгом и любым значением
    """
    def __init__(self, tag):
        super(Tag, self).__init__()
        self.tag = tag

    def __call__(self, tokens, pos, index_offset=0):
        if pos < len(tokens) and tokens[pos][1] is self.tag:
            text_pos = tokens[pos][2]
            return Result(tokens[pos][0], pos + 1, text_pos, index_offset)
        else:
            return None


class Reserved(BaseParser):
    """
    Парсит зарезервированное с указанным значением
    """
    def __init__(self, value, tag):
        super(Reserved, self).__init__()
        self.value = value
        self.tag = tag


    def __call__(self, tokens, pos, index_offset=0):
        condition = pos < len(tokens) and \
                tokens[pos][0] == self.value and \
                tokens[pos][1] is self.tag
        if condition:
            return Result(tokens[pos][0], pos + 1, tokens[pos][2], index_offset)
        else:
            return None

class Not(BaseParser):
    """
    Поглащает токены до тех пор, пока не появится соответствующий парсеру токен
    """
    def __init__(self, parser):
        super(Not, self).__init__()
        self.parser = parser

    def __call__(self, tokens, pos, index_offset=0):
        if len(tokens) <= pos:
            return None
        result = self.parser(tokens, pos, index_offset)
        content = None
        if not result:
            text_pos = tokens[pos][2]
            content = tokens[pos][0]
            return Result(content, pos+1, text_pos, index_offset)
        return None

    def __repr__(self):
        return 'Not(%s)' % (str(self.parser))

class Concat(BaseParser):
    """
    Объединение двух парсеров. Результатом будет кортеж результата левого и правого парсера.
    """
    def __init__(self, left, right):
        super(Concat, self).__init__()
        self.left = left
        self.right = right

    def __call__(self, tokens, pos, index_offset=0):
        left_result = self.left(tokens, pos, index_offset)
        if left_result:
            if left_result.pos >= len(tokens):
                return None
            right_result = self.right(tokens, left_result.pos, index_offset)
            if right_result:
                combined_value = (left_result.value, right_result.value)
                text_pos = tokens[pos][2]
                return Result(combined_value, right_result.pos, text_pos, index_offset)
        return None

class Exp(BaseParser):
    """
    Разделяет токены первым парсером, , а потом, по остатку, пробегает разделителем, комбинирует данные значения,
    после чего применяет в цикле то же самое для свежеполученного результата,
    позволяя, тем самым, вместо рекурсии использовать цикл
    """
    def __init__(self, parser, separator):
        super(Exp, self).__init__()
        self.parser = parser
        self.separator = separator

    def __call__(self, tokens, pos, index_offset=0):
        result = self.parser(tokens, pos, index_offset)

        def process_next(parsed, pos, index_offset=0):
            (sepfunc, right) = parsed
            return sepfunc(result.value, right, pos, index_offset)
            # return sepfunc(result.value, right, pos, index_offset)
        next_parser = self.separator + self.parser ^ process_next

        next_result = result
        while next_result:
            next_result = next_parser(tokens, result.pos, index_offset)
            if next_result:
                result = next_result
        return result

class ExpCondition(BaseParser):

    def __init__(self, parser, condition, separator):
        super(ExpCondition, self).__init__()
        self.parser = parser
        self.condition = condition
        self.separator = separator

    def __call__(self, tokens, pos, index_offset=0):
        check_condition = self.condition(tokens, pos, index_offset)
        if not check_condition:
            return None

        result = self.parser(tokens, pos, index_offset)

        def process_next(parsed, pos, index_offset=0):
            (sepfunc, right) = parsed
            # return sepfunc(result.value, right, tokens[pos][2])
            return sepfunc(result.value, right, pos, index_offset)

        next_parser = self.separator + self.parser ^ process_next
        next_condition = self.separator + self.condition

        next_result = result
        while next_result:
            check_condition = next_condition(tokens, result.pos, index_offset)
            if not check_condition:
                break

            next_result = next_parser(tokens, result.pos, index_offset)
            if next_result:
                result = next_result
        return result

class Alternate(BaseParser):
    """
    Возвращает результат левого парсера, или, если левый пуст, правый
    """
    def __init__(self, left, right):
        super(Alternate, self).__init__()
        self.left = left
        self.right = right

    def __call__(self, tokens, pos, index_offset=0):
        if pos >= len(tokens):
            return None

        left_result = self.left(tokens, pos, index_offset)
        if left_result:
            return left_result
        else:
            right_result = self.right(tokens, pos, index_offset)
            return right_result

class Opt(BaseParser):
    """
    Если результат парсера None, то положительный ответ всё равно приходит, но позиция токенов не именяется
    (для раелизации опциональных ветвей типа Else)
    """
    def __init__(self, parser):
        super(Opt, self).__init__()
        self.parser = parser

    def __call__(self, tokens, pos, index_offset=0):
        result = self.parser(tokens, pos, index_offset)
        if result:
            return result
        else:

            return Result(None, pos, tokens[pos][2], index_offset)


class Rep(BaseParser):
    """
    Принимает парсер в цикле, пока не выйдет из строя
    """
    def __init__(self, parser):
        super(Rep, self).__init__()
        self.parser = parser

    def __call__(self, tokens, pos, index_offset=0):
        results = []
        result = self.parser(tokens, pos, index_offset)
        while result:
            results.append(result.value)
            pos = result.pos
            result = self.parser(tokens, pos, index_offset)
        text_pos = None
        text_pos = tokens[pos-1][2]
        return Result(results, pos, text_pos, index_offset)

class Process(BaseParser):
    """
    Комбинатор, который, в случае спеха, модифицирует результат переданной ему функцией
    """
    def __init__(self, parser, function):
        super(Process, self).__init__()
        self.parser = parser
        self.function = function

    def __call__(self, tokens, pos, index_offset=0):
        result = self.parser(tokens, pos, index_offset)
        if result:
            result.text_pos = tokens[pos][2]
            result.value = self.function(result.value, tokens[pos][2], index_offset)
            return result

class Lazy(BaseParser):
    """
    Отложенный вызов функции-парсера. Полезен с рекурсивными парсерами
    """
    def __init__(self, parser_func):
        super(Lazy, self).__init__()
        self.parser = None
        self.parser_func = parser_func

    def __call__(self, tokens, pos, index_offset=0):
        if not self.parser:
            self.parser = self.parser_func()
        return self.parser(tokens, pos, index_offset)

class Phrase(BaseParser):
    """
    Не выполнится, пока не поглотит все токены. Обычно, корень дерева парсеров (комбинаторов)
    """
    def __init__(self, parser):
        super(Phrase, self).__init__()
        self.parser = parser

    def __call__(self, tokens, pos, index_offset=0):
        result = self.parser(tokens, pos, index_offset)
        if result and result.pos == len(tokens):
            return result
        else:
            return None


class Condition(BaseParser):
    """
    Парсер будет выполняться в том случае, если так же будет выполняться его условие.
    Условие при этом не отъедает токены
    """
    def __init__(self, parser, condition):
        super(Condition, self).__init__()
        self.parser = parser
        self.condition = condition

    def __call__(self, tokens, pos, index_offset=0):
        condition_res = self.condition(tokens, pos, index_offset)
        result = None
        if condition_res:
            result = self.parser(tokens, pos, index_offset)
        return result

class RepCondition(BaseParser):
    """
    Принимает парсер в цикле, пока не выйдет из строя, при этом проверяя, выполняется ли условие
    Условие при этом не отъедает токены
    """
    def __init__(self, parser, condition):
        super(RepCondition, self).__init__()
        self.parser = parser
        self.condition = condition

    def __call__(self, tokens, pos, index_offset=0):
        results = []
        condition_res = self.condition(tokens, pos, index_offset)
        if not condition_res: return None

        result = self.parser(tokens, pos, index_offset)
        while result:
            results.append(result.value)
            pos = result.pos
            condition_res = self.condition(tokens, pos, index_offset)
            if not condition_res:
                break
            result = self.parser(tokens, pos, index_offset)
        text_pos = None
        text_pos = tokens[pos-1][2]
        if not results:
            return None
        return Result(results, pos, text_pos, index_offset)



class Check(BaseParser):
    """
    Не даёт пройти выполнение, если условие не выполняется
    """
    def __init__(self, parser, cond):
        super(Check, self).__init__()
        self.parser = parser
        self.cond = cond

    def __call__(self, tokens, pos, index_offset=0):
        result = self.parser(tokens, pos, index_offset)
        if result and self.cond(result):
            return result
        else:
            return None