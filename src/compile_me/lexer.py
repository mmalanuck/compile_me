__author__ = 'cjay'

import re


def lex_me(characters, token_exprs):
    pos = 0
    tokens = []
    while pos < len(characters):
        match = None
        for token_expr in token_exprs:
            pattern, tag = token_expr
            # TODO: cache compiled regex
            regex = None
            match = None
            try:
                regex = re.compile(pattern, re.UNICODE)
                match = regex.match(characters, pos)
                if match:
                    text = match.group(0)
                    if tag:
                        token = (text, tag, pos)
                        tokens.append(token)
                    break
            except Exception as e:
                print e, pattern
                raise  e
        if not match:
            raise Exception('Illegal character: %s in position %d' % (characters[pos], pos))
        else:
            pos = match.end(0)
    return tokens