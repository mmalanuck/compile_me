# -*- coding: utf8 -*-
from compile_me.ast import BaseNode, Node

__author__ = 'cjay'


class Comment(BaseNode):
    """
    Нода комментария
    """
    state = 3

    def __init__(self, content, pos):
        super(Comment, self).__init__(pos)
        self.content = content

    def get_name(self):
        return "Comment"

    def __repr__(self):
        return '|%d| CommentNode: %s...' % (self.pos, self.content[:10])

    def to_text(self):
        return "// %s" % self.content


class Noun(Node):
    """
    Нода существительного
    """
    state = 10

    def __repr__(self):
        return '|%d|  NounNode: %s' % (self.pos, self.name)


    def get_name(self):
        return self.name

    def get_name_size(self):
        return len(self.name)

    def to_text(self):
        return "<%s>" % self.name


class NounCreation(BaseNode):
    """
    Нода описания существительного
    """

    state = 7

    def __init__(self, noun, content, pos):
        super(NounCreation, self).__init__(pos)
        self.noun = noun
        self.content = content


    def get_name_size(self):
        return len(self.noun.name)


    def __repr__(self):
        return '|%d| NounCreation: %s (%s...)' % (self.pos, self.noun.name, self.content)

    def __iter__(self):
        for i in self.content:
            yield i

    def get_name(self):
        return self.noun.name

    def to_text(self):
        res = []
        for i in self.content:
            if isinstance(i, BaseNode):
                res.append(i.to_text())
                continue
            if isinstance(i, list):
                for j in i:
                    res.append(j.to_text())
                continue
            if isinstance(i, str):
                res.append(i)
        return "%s - %s" % (self.noun.to_text(), " ".join(res))
        # return "%s - %s" % (self.noun.to_text(), " ".join([i.to_to_text() for i in self.desc]))

class Package(Node):
    """
    Нода пакета
    """

    state = 8

    def __repr__(self):
        return '|%d| Package(%s)' % (self.pos, self.name)

    def get_name(self):
        return self.name

    def to_text(self):
        return "[%s]" % self.name

class PackageCreation(BaseNode):
    """
    Нода создания пакета
    """

    state = 4

    def __init__(self, package, content, pos):
        super(PackageCreation, self).__init__(pos)
        self.package = package
        self.content = content

    def __repr__(self):
        items = ",".join([str(i) for i in self.content])
        return '|%d|  PackageCreation(%s): [%s]' % (self.pos, self.package.name, items)

    def get_name(self):
        return self.package.name

    def __iter__(self):
        for i in self.content:
            yield i

    def __len__(self):
        return len(self.content)


    def to_text(self):
        lines = [i.to_text() for i in self.content]
        lines_text = "\n".join(lines)
        return "%s\n%s" % (self.package.to_text(), lines_text)


class Precedent(Node):
    """
    Нода Прецедента
    """

    state = 9

    def __repr__(self):
        return '|%d| Precedent(%s)' % (self.pos, self.name)

    def get_name(self):
        return self.name

    def to_text(self):
        return "//%s//" % self.name


class UnrecognizedLine(BaseNode):
    """
    Строка с неструктурированой информацией
    """

    def __init__(self, content, pos):
        super(UnrecognizedLine, self).__init__(pos)
        self.content = content

    def __repr__(self):
        return '|%d| ----- (%d nodes) ------' % (self.pos, len(self.content))

    def __iter__(self):
        for i in self.content:
            yield i

    def get_name(self):
        return "-------"

    def to_text(self):
        return " ".join([i.to_text() for i in self.content if isinstance(i, BaseNode)])


class PrecedentCreation(BaseNode):
    """
    Нода определения прецедента
    """
    state = 5

    def __init__(self, precedent, content, pos):
        super(PrecedentCreation, self).__init__(pos)
        self.precedent = precedent
        self.content = content


    def __iter__(self):
        for i in self.content:
            yield i

    def __repr__(self):
        return '|%d|  PrecDefd: %s (%d items))' % \
               (self.pos, self.precedent.name, len(self.content))

    def get_name(self):
        return self.precedent.name

    def to_text(self):
        items = []
        for i in self.content:
            items.append(self._stringify_node(i))

        items_text = "/n".join(items)
        return "%s\n%s" % (self.precedent.to_text(), items_text)

class ASTTree:
    """
    Данный класс позволяет проводить поиск по дереву, искать ноды по положению в тексте
    """

    def __init__(self, nodes=None):
        if not nodes:
            nodes = []
        self.nodes = nodes

    def __eq__(self, other):
        return self.nodes == other.nodes

    def __ne__(self, other):
        return not self.__eq__(other)

    def __iter__(self):
        for i in self.nodes:
            yield i


    def deeper_child_pos(self, node):
        """
        Возвращаем крайнюю границу ноды (положение его самого глубокого и последнего дочернего элемента)
        :param node: нода, границу которого нужно найти
        :return: int
        """
        if not node:
            return None
        if not isinstance(node, BaseNode):
            return None
        items = [i for i in node]
        item = None
        if items:
            item = items[-1]
        if item:
            res = self.deeper_child_pos(item)
            if res:
                return res
        return node.pos

    def get_all_nodes(self, node):
        if not node:
            return []
        nodes = []
        node_type = type(node)

        if node_type == str:
            return [node]
        elif node_type == Noun:
            return [node]
        elif node_type == Package:
            return [node]
        elif node_type == Precedent:
            return [node]
        elif node_type == NounCreation:
            nodes.append(node.noun)
        elif node_type == PrecedentCreation:
            nodes.append(node.precedent)
        elif node_type == PackageCreation:
            nodes.append(node.package)
        elif isinstance(node, BaseNode):
            nodes.append(node)

        for i in node:
            if not i:
                continue
            if isinstance(i, (BaseNode, list)):
                inner_items = self.get_all_nodes(i)
                nodes.extend(inner_items)
        return nodes


    def find_by_range(self, start, end=None, node=None):
        """
        Вернуть список всех нод, расположенных в заданном диапазоне текста
        :param start: int
        :param end: int
        :param node: BaseNode
        :return: list
        """
        nodes = self.get_all_nodes(self.nodes)

        result = []

        for item in nodes:

            if not item:
                continue

            if not isinstance(item, BaseNode):
                continue

            # Если нода вышла за правую границу, выходим.
            if end and item.pos > end:
                break
            # Если положение ноды меньше начала поиска
            if item.pos < start:
                # И при этом, его правая граница меньше начала поиска
                deeper_pos = self.deeper_child_pos(item)
                if deeper_pos < start:
                    continue  # пропускаем этотэлемент

            result.append(item)
            # childs = self.find_by_range(start, end, item)
            # if childs:
            # result.extend(childs)
        if end is None:
            result = [i for i in result if (i.pos >= start)]
        else:
            result = [i for i in result if ((i.pos >= start) and (i.pos <= end))]
        return result


    def find_by_textpos(self, pos, node=None):
        """
        Возвращает ноды (вместе с дочерними), которые находятся на текущей позиции в тексте. Используется для брекрамба
        :param pos: int - позиция в тексте
        :param node: syntax.BaseNode - нода, внутри которой искать
        :return: list
        """
        result_item = None
        nodes = []
        if not node:
            nodes = self.nodes
        else:
            nodes = []
            for i in node:
                if not i:
                    continue
                if isinstance(i, list):
                    nodes.extend(i)
                else:
                    nodes.append(i)

        for item in nodes:
            if not item:
                continue
            if isinstance(item, str):
                continue
            if item.pos > pos:
                break
            result_item = item
        res = None
        if result_item:
            if isinstance(result_item, PackageCreation):
                res = [result_item.package]
            elif isinstance(result_item, PrecedentCreation):
                res = [result_item.precedent]
            elif isinstance(result_item, NounCreation):
                res = [result_item.noun]
            else:
                res = [result_item]
            child_res = self.find_by_textpos(pos, result_item)
            if child_res:
                child_res = [i for i in child_res if i]
            if child_res:
                res.extend(child_res)
        return res

    def to_text(self):
        return "\n".join([i.to_text() for i in self.nodes])