# -*- coding: utf8 -*-

__author__ = 'cjay'

from compile_me.iconix.lexem import *
from compile_me.combinators import *
from compile_me.iconix.ast import *


# Basic parsers
def keyword(kw):
    return Reserved(kw, RESERVED)


def any_text():
    return Alternate(TextNode, SpaceNode)


TextNode = Tag(TEXT)
EolNode = Tag(ENDLINE)
SpaceNode = Tag(SPACE)


def parser(tokens, index_offset=0):
    """
    Парсер иконикс-синтаксиса
    :param tokens:
    :return:
    """
    if len(tokens) == 0:
        return Result([], 0, 0)
    ast = iconix_entry()(tokens, 0, index_offset)
    return ast


def modify_name(parsed, tokens, offset=0):
    first_text, items = parsed
    result = "%s" % first_text
    for i in items:
        result += i
    return result


def name_content_noun():
    return Alternate(
        TextNode + Rep(TextNode | keyword("<") | keyword("/") | keyword("[") | keyword("]") | keyword(
            "-") | SpaceNode) ^ modify_name,
        TextNode
    )


def noun():
    def modify(parsed, pos, offset):
        (_, name), _ = parsed
        result = Noun(name, pos + offset)
        return result

    return keyword("<") + name_content_noun() + keyword(">") ^ modify


def name_content_prec():
    return Alternate(
        TextNode + Rep(TextNode | keyword("<") | keyword(">") | keyword("[") | keyword("]") | keyword(
            "-") | SpaceNode) ^ modify_name,
        TextNode
    )


def precedent():
    def modify(parsed, pos, offset=0):
        (_, name), _ = parsed
        result = Precedent(name, pos + offset)
        return result

    return keyword("/") + name_content_prec() + keyword("/") ^ modify


def name_content_pack():
    return Alternate(
        TextNode + Rep(TextNode | keyword("<") | keyword(">") | keyword("[") | keyword("/") | keyword(
            "-") | SpaceNode) ^ modify_name,
        TextNode
    )


def package():
    def modify(parsed, pos, offset=0):
        (_, name), _ = parsed
        result = Package(name, pos + offset)
        return result

    return keyword("[") + name_content_pack() + keyword("]") ^ modify


def meaning_line():
    def modify(parsed, pos, offset=0):
        first_item, other_items = parsed
        items = [first_item]
        for i in other_items:
            if isinstance(items[-1], str) and isinstance(i, str):
                items[-1] = items[-1] + i
            else:
                items.append(i)

        result = UnrecognizedLine(items, pos + offset)
        return result

    def modify2(parsed, pos, offset=0):
        return UnrecognizedLine([parsed], pos + offset)

    return Alternate(any_text() + Rep(
        TextNode | noun() | precedent() | package() | keyword("<") | keyword(">") | keyword("/") | keyword(
            "[") | keyword("]") | keyword("-") | SpaceNode) ^ modify, any_text() ^ modify2)


def noun_creation():
    def modify(parsed, pos, offset=0):
        ((((noun, _), _), _), line) = parsed
        result = NounCreation(noun, line, pos + offset)
        return result

    return noun() + Opt(SpaceNode) + keyword("-") + Opt(SpaceNode) + meaning_line() ^ modify


def precedent_creation():
    def noun_and_meaning():
        return noun_creation() | comment() | meaning_line() | EolNode


    def modify1(parsed, pos, offset=0):
        (((precedent, _), items_with_eol), last_item) = parsed

        content = []
        if items_with_eol:
            content.extend([i[0] for i in items_with_eol])

        if last_item:
            content.append(last_item)

        res = PrecedentCreation(precedent, content, pos + offset)
        return res

    def modify2(parsed, pos, offset=0):
        ((precedent, _), items) = parsed
        content = [i for i in items if i and isinstance(i, BaseNode)]
        res = PrecedentCreation(precedent, content, pos + offset)
        return res

    def modify3(parsed, pos, offset=0):
        (precedent, _) = parsed
        res = PrecedentCreation(precedent, [], pos + offset)
        return res

    return Alternate(
            precedent() + EolNode + Rep(noun_and_meaning()) ^ modify2
        ,
        precedent() + EolNode ^ modify3,
    )

def package_creation():
    def prec_and_noun_and_line():
        return precedent_creation() | noun_creation() | comment() | meaning_line() | noun() | EolNode

    def modify1(parsed, pos, offset=0):
        # print("modify 1")
        (((pack, _), items), last_item) = parsed
        content = []
        content.extend([i for i in items if i != "\n"])
        if last_item:
            content.append(last_item)
        res = PackageCreation(pack, content, pos + offset)
        return res

    def modify2(parsed, pos, offset=0):
        # print("Modify 2")
        # print(parsed)
        ((pack, _), items) = parsed
        content = [i for i in items if i != "\n"]
        res = PackageCreation(pack, content, pos + offset)
        return res

    def modify3(parsed, pos, offset=0):
        # print("modify 3")
        # print("parsed", parsed)
        (pack, _) = parsed
        res = PackageCreation(pack, [], pos + offset)
        return res

    def modify4(parsed, pos, offset=0):
        (pack, _) = parsed
        res = PackageCreation(pack, [], pos + offset)
        return res

    return Alternate(
            Alternate(
                package() + EolNode + Rep(prec_and_noun_and_line()) + prec_and_noun_and_line() ^ modify1,
                package() + EolNode + Rep(prec_and_noun_and_line()) ^ modify2
            ),
        package() + EolNode ^ modify3
    )


def comment():
    def modify1(parsed, pos, offset=0):
        ((_, items), _) = parsed
        res = "".join(items)
        return Comment(res, pos + offset)

    def modify2(parsed, pos, offset=0):
        (_, items) = parsed
        res = "".join(items)
        return Comment(res, pos + offset)

    return Alternate(keyword("/") + keyword("/") + Rep(
        any_text() | keyword("/") | keyword('<') | keyword(">") | keyword("[") | keyword("]") | keyword(
            "-")) + EolNode ^ modify1,
                     keyword("/") + keyword("/") + Rep(
                         any_text() | keyword("/") | keyword('<') | keyword(">") | keyword("[") | keyword(
                             "]") | keyword("-")) ^ modify2)


def parser(tokens, index_offset=0):
    """
    Парсер иконикс-синтаксиса
    :param tokens:
    :return:
    """
    if len(tokens) == 0:
        return Result([], 0, 0)
    ast = main_parser()(tokens, 0, index_offset)
    return ast


def null_line():
    return EolNode

def trash_line():
    return keyword("/") | keyword("[") | keyword("]") | keyword("<") | keyword(">") | keyword("-") | SpaceNode | TextNode

def main_parser():
    return Rep(comment() | noun_creation() | precedent_creation() | package_creation() | meaning_line() | null_line() | trash_line())


def iconix_entry():
    def modify(parsed, pos, offset=0):
        return ASTTree(parsed)

    return main_parser() ^ modify
