# -*- coding: utf8 -*-

__author__ = 'cjay'

import unittest
import os

from compile_me.iconix.ast import *
from compile_me.iconix.lexem import *
from compile_me.iconix.grammar import iconix_entry


class TestASTTree(unittest.TestCase):
    def test_creation(self):
        nodes = [
            Node("A", 0),
            Node("B", 1),
            Node("C", 3),
            Node("D", 4),
        ]
        tree = ASTTree(nodes)
        self.assertEqual(tree.nodes, nodes)

    def test_equal(self):
        nodes = [
            Node("A", 0),
            Node("B", 1),
            Node("C", 3),
            Node("D", 4),
        ]
        tree = ASTTree(nodes)
        nodes2 = [
            Node("A", 0),
            Node("B", 1),
            Node("C", 3),
            Node("D", 4),
        ]
        tree2 = ASTTree(nodes2)
        # self.assertEqual(tree, tree2)

        nodes2 = [
            Node("B", 1),
            Node("C", 3),
            Node("D", 4),
        ]
        tree2 = ASTTree(nodes2)
        self.assertNotEqual(tree, tree2)

    def test_find_node_by_textpos(self):
        nodes = [
            Node("A", 0),
            Node("B", 1),
            Node("C", 3),
            Node("D", 4),
        ]
        tree = ASTTree(nodes)
        node = tree.find_by_textpos(2)
        self.assertEqual(node, [nodes[1]])


        f_path = os.path.join(os.path.dirname(__file__), "test_content.icx")

        with open(f_path) as file:
            code = file.read()
            parser = iconix_entry()
            result = parser(lex(code), 0)
            items = result.value.find_by_textpos(411)
            # print("->", len(items))
            # for i in items:
            #     print("--->", type(i), i.pos)

    def test_deeper_child_pos(self):
        """
        ЧТО ЗА ТЕСТ?
        :return:
        """
        # TODO: понять, что за тест
        f_path = os.path.join(os.path.dirname(__file__), "test_content.icx")
        with open(f_path) as file:
            code = file.read()
            parser = iconix_entry()
            tokens = lex(code)
            tree = parser(tokens, 0).value
            # dp = tree.deeper_child_pos(tree.root_nodes[0])
            # self.assertEqual(dp, 2)
            # dp = tree.deeper_child_pos(tree.root_nodes[1])
            # self.assertEqual(dp, 17)
            # dp = tree.deeper_child_pos(tree.root_nodes[2])
            # self.assertEqual(dp, 328)

    def test_find_by_range(self):
        nodes = [
            Node("A", 0),
            Node("B", 1),
            Node("C", 3),
            Node("D", 4),
        ]
        tree = ASTTree(nodes)
        res = tree.find_by_range(1, 3)
        self.assertEqual(len(res), 2)