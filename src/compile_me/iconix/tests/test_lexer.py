# -*- coding: utf8 -*-
__author__ = 'cjay'

import unittest

from compile_me.iconix import lexem

class TestLexer(unittest.TestCase):
    def lexer_test(self, code, expected):
        actual = lexem.lex(code)
        self.assertEquals(expected, actual)

    def test_trim_white_space(self):
        str = "   123456 78"
        res = lexem.trim_white_space(str)
        self.assertEqual(res, "123456 78")
        str = "   123456 78  "
        res = lexem.trim_white_space(str)
        self.assertEqual(res, "123456 78")
        str = "   [123456 78]  "
        res = lexem.trim_white_space(str)
        self.assertEqual(res, "[123456 78]")

    def test_empty(self):
        self.lexer_test('', [])

    def test_comment(self):
        code = "// this is a comment"
        expected = [('/', 'RESERVED', 0), ('/', 'RESERVED', 1), (" ", 'SPACE', 2), ('this is a comment', 'TEXT', 3)]
        self.lexer_test(code, expected)

        code = "// <this> is a comment"
        expected = [('/', 'RESERVED', 0), ('/', 'RESERVED', 1),(" ", "SPACE", 2), ('<', 'RESERVED', 3), ('this', 'TEXT', 4), ('>', 'RESERVED', 8),(" ", "SPACE", 9) , ('is a comment', 'TEXT', 10)]
        a = lexem.lex(code)
        self.lexer_test(code, expected)

        code = "// comment1\n// comment2"
        expected = [('/', 'RESERVED', 0), ('/', 'RESERVED', 1), ('comment1', 'TEXT', 2),("\n", "ENDLINE", 11),("", "STARTLINE", 12) , ("/", "RESERVED", 12), ("/", "RESERVED", 13), ('comment2', 'TEXT', 14), ('', 'ENDFILE', 14)]
        # self.lexer_test(code, expected)
        # TODO: ДОПИСАТЬ ТЕСТ
