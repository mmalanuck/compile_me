# -*- coding: utf8 -*-


__author__ = 'cjay'

import unittest
from compile_me.iconix.ast import ASTTree
from compile_me.iconix.lexem import *
from compile_me.iconix import grammar as ng
from compile_me.iconix import ast


class TestNewGrammarParser(unittest.TestCase):
    def parser_test(self, code, parser, expected, index_offset=0):
        tokens = lex(code)
        result = parser(tokens, 0, index_offset)
        self.assertNotEquals(None, result)
        self.assertEquals(expected, result.value)
        return result

    def test_name_content_grammars(self):
        code = "this is a mynoun_gramm"
        parser = ng.name_content_noun()
        expected = 'this is a mynoun_gramm'
        self.parser_test(code, parser, expected)

        parsers = [ng.name_content_noun(), ng.name_content_prec(), ng.name_content_pack()]
        for i in parsers:
            # Должны начинаться только с текста
            self.assertIsNone(i(lex("> this is a - [grp] name"), 0))
            self.assertIsNone(i(lex("< this is a - [grp] name"), 0))
            self.assertIsNone(i(lex("[ this is a - [grp] name"), 0))
            self.assertIsNone(i(lex("] this is a - [grp] name"), 0))
            self.assertIsNone(i(lex("- this is a - [grp] name"), 0))
            self.assertIsNone(i(lex("/ this is a - [grp] name"), 0))
            self.assertIsNone(i(lex(" this is a - [grp] name"), 0))

        # Принимает все символы до ">"
        parser = ng.name_content_noun()
        self.parser_test("this is a - [grp] name", parser, 'this is a - [grp] name')
        self.parser_test("this is a /prec/ name", parser, "this is a /prec/ name")
        self.parser_test("this is a /prec name", parser, "this is a /prec name")
        self.parser_test("this is a <noun< name", parser, "this is a <noun< name")
        self.parser_test("this is a -noun- name", parser, "this is a -noun- name")
        self.parser_test("this is a <noun> name", parser, "this is a <noun")

        # Принимает все символы до "]"
        parser = ng.name_content_pack()
        self.parser_test("this is a - <grp> name", parser, 'this is a - <grp> name')
        self.parser_test("this is a /prec/ name", parser, "this is a /prec/ name")
        self.parser_test("this is a /prec name", parser, "this is a /prec name")
        self.parser_test("this is a [noun[ name", parser, "this is a [noun[ name")
        self.parser_test("this is a -noun- name", parser, "this is a -noun- name")
        self.parser_test("this is a [noun] name", parser, "this is a [noun")

        # Принимает все символы до "/"
        parser = ng.name_content_prec()
        self.parser_test("this is a - <grp> name", parser, 'this is a - <grp> name')
        self.parser_test("this is a [prec] name", parser, "this is a [prec] name")
        self.parser_test("this is a <prec> name", parser, "this is a <prec> name")
        self.parser_test("this is a [noun[ name", parser, "this is a [noun[ name")
        self.parser_test("this is a -noun- name", parser, "this is a -noun- name")
        self.parser_test("this is a /noun/ name", parser, "this is a ")

    def test_noun(self):
        code = "<this is a name>"
        parser = ng.noun()
        expected = ast.Noun('this is a name', 0)
        self.parser_test(code, parser, expected)

        txt = "<this is a noun>"
        self.parser_test(txt, parser, ast.Noun(txt[1:-1], 0))

        # Можно внутри добавлять левые символы ))
        txt = "<this < is a noun>"
        self.parser_test(txt, parser, ast.Noun(txt[1:-1], 0))

        txt = "<this [ ] / - < is a noun>"
        self.parser_test(txt, parser, ast.Noun(txt[1:-1], 0))

    def test_precedent(self):
        code = "/this is a name/"
        parser = ng.precedent()
        expected = ast.Precedent('this is a name', 0)
        self.parser_test(code, parser, expected)

        txt = "/this is a precedent/"
        self.parser_test(txt, parser, ast.Precedent(txt[1:-1], 0))

        # Можно внутри добавлять левые символы ))
        txt = "/this [ ] < is a prec>/"
        self.parser_test(txt, parser, ast.Precedent(txt[1:-1], 0))

    def test_package(self):
        code = "[this is a name]"
        parser = ng.package()
        expected = ast.Package('this is a name', 0)
        self.parser_test(code, parser, expected)

        txt = "[this is a package]"
        self.parser_test(txt, parser, ast.Package(txt[1:-1], 0))

        # Можно внутри добавлять левые символы ))
        txt = "[this / // asda /is a package/]"
        self.parser_test(txt, parser, ast.Package(txt[1:-1], 0))

    def test_meaning_line(self):
        code = "T"
        parser = ng.meaning_line()
        expected = ast.UnrecognizedLine(["T"], 0)
        self.parser_test(code, parser, expected)

        code = " "
        parser = ng.meaning_line()
        expected = ast.UnrecognizedLine([" "], 0)
        self.parser_test(code, parser, expected)

        code = "This is some <meaning> line"
        parser = ng.meaning_line()
        expected = ast.UnrecognizedLine(["This is some ", ast.Noun("meaning", 13), " line"], 0)
        self.parser_test(code, parser, expected)

        code = "This is some [meaning] line"
        parser = ng.meaning_line()
        expected = ast.UnrecognizedLine(["This is some ", ast.Package("meaning", 13), " line"], 0)
        self.parser_test(code, parser, expected)

        code = "This is some /meaning/ line"
        parser = ng.meaning_line()
        expected = ast.UnrecognizedLine(["This is some ", ast.Precedent("meaning", 13), " line"], 0)

        self.parser_test(code, parser, expected)

    def test_noun_creation(self):
        code = "<This> - hello"
        parser = ng.noun_creation()
        expected = ast.NounCreation(ast.Noun("This", 0), ast.UnrecognizedLine(["hello"], 9), 0)
        self.parser_test(code, parser, expected)

        code = "<This> -hello"
        parser = ng.noun_creation()
        expected = ast.NounCreation(ast.Noun("This", 0), ast.UnrecognizedLine(["hello"], 8), 0)
        self.parser_test(code, parser, expected)

        code = "<This>-hello"
        parser = ng.noun_creation()
        expected = ast.NounCreation(ast.Noun("This", 0), ast.UnrecognizedLine(["hello"], 7), 0)
        self.parser_test(code, parser, expected)

        code = "<This>-hello <man>"
        parser = ng.noun_creation()
        expected = ast.NounCreation(ast.Noun("This", 0),
                                           ast.UnrecognizedLine(["hello ", ast.Noun("man", 13)], 7), 0)
        self.parser_test(code, parser, expected)

        code = "<This>-hello <man>!"
        parser = ng.noun_creation()
        expected = ast.NounCreation(ast.Noun("This", 0),
                                           ast.UnrecognizedLine(["hello ", ast.Noun("man", 13), "!"], 7), 0)
        self.parser_test(code, parser, expected)

        code = "<This>-hello <man> /prec/ !"
        parser = ng.noun_creation()
        expected = ast.NounCreation(ast.Noun("This", 0),
                                           ast.UnrecognizedLine([
                                               "hello ",
                                               ast.Noun("man", 13),
                                               " ",
                                               ast.Precedent("prec", 19),
                                               " !"
                                           ], 7), 0)
        self.parser_test(code, parser, expected)

    def test_prec_creation(self):
        code = "/prec/\n"
        parser = ng.precedent_creation()
        expected = ast.PrecedentCreation(ast.Precedent("prec", 0), [], 0)
        self.parser_test(code, parser, expected)

        code = "/prec/\n<This>-hello"
        expected = ng.PrecedentCreation(ng.Precedent("prec", 0), [
            ng.NounCreation(ng.Noun("This", 7), ng.UnrecognizedLine([
                "hello"
            ], 14), 7)
        ], 0)
        self.parser_test(code, parser, expected)

        code = "/prec/\n<This>-hello\n"
        self.parser_test(code, parser, expected)

        code = "/prec/\n<This>-hello\n<Some> - hello\n"
        expected = ng.PrecedentCreation(ng.Precedent("prec", 0), [
            ng.NounCreation(ng.Noun("This", 7), ng.UnrecognizedLine([
                "hello"
            ], 14), 7),
            ng.NounCreation(ng.Noun("Some", 20), ng.UnrecognizedLine([
                "hello"
            ], 29), 20)
        ], 0)
        self.parser_test(code, parser, expected)

        code = "/prec/\nAny other <text_node>\n<Some> - hello\n"
        expected = ng.PrecedentCreation(ng.Precedent("prec", 0), [
            ng.UnrecognizedLine(["Any other ", ng.Noun("text_node", 17)], 7),
            ng.NounCreation(ng.Noun("Some", 29), ng.UnrecognizedLine([
                "hello"
            ], 38), 29)
        ], 0)
        self.parser_test(code, parser, expected)

        code = "/prec/\nAny other <text_node>\n<Some> - hello"
        expected = ng.PrecedentCreation(ng.Precedent("prec", 0), [
            ng.UnrecognizedLine(["Any other ", ng.Noun("text_node", 17)], 7),
            ng.NounCreation(ng.Noun("Some", 29), ng.UnrecognizedLine([
                "hello"
            ], 38), 29)
        ], 0)
        self.parser_test(code, parser, expected)

        code = "/prec/\nAny other <text_node>"
        expected = ng.PrecedentCreation(ng.Precedent("prec", 0), [
            ng.UnrecognizedLine(["Any other ", ng.Noun("text_node", 17)], 7),
        ], 0)
        self.parser_test(code, parser, expected)

    def test_package_creation(self):
        code = "[pac11kage]\n\n\n\n"
        parser = ng.package_creation()
        expected = ng.PackageCreation(ng.Package("pac11kage", 0), [], 0)
        self.parser_test(code, parser, expected)


        code = "[package]\n"
        parser = ng.package_creation()
        expected = ng.PackageCreation(ng.Package("package", 0), [], 0)
        self.parser_test(code, parser, expected)

        code = "[package]\n\nAny other <text_node>\n"
        parser = ng.package_creation()
        expected = ng.PackageCreation(ng.Package("package", 0), [
            ng.UnrecognizedLine([
                "Any other ",
                ng.Noun("text_node", 21)
            ], 11),

        ], 0)
        self.parser_test(code, parser, expected)

        code = "[package]\nAny other <text_node>\n"
        parser = ng.package_creation()
        expected = ng.PackageCreation(ng.Package("package", 0), [
            ng.UnrecognizedLine([
                "Any other ",
                ng.Noun("text_node", 20)
            ], 10),
        ], 0)
        self.parser_test(code, parser, expected)

        code = "[package]\n<Any> - other <text_node>\n"
        parser = ng.package_creation()
        expected = ng.PackageCreation(ng.Package("package", 0), [
            ng.NounCreation(ng.Noun("Any", 10),
                                   ng.UnrecognizedLine([
                                       "other ",
                                       ng.Noun("text_node", 24)],
                                       18),
                                   10),
            ],
            0)
        self.parser_test(code, parser, expected)

        code = "[package]\n<Any> - other <text_node>"
        parser = ng.package_creation()
        expected = ng.PackageCreation(ng.Package("package", 0), [
            ng.NounCreation(ng.Noun("Any", 10),
                                   ng.UnrecognizedLine([
                                       "other ",
                                       ng.Noun("text_node", 24)
                                   ], 18), 10)
        ], 0)
        self.parser_test(code, parser, expected)

        code = "[package]\n<Any> - other <text_node> here\n/precedent/\n<new prec-noun>- is a simple <test> with some /precedents/ for a while!"
        parser = ng.package_creation()
        expected = ng.PackageCreation(ng.Package("package", 0), [], 0)
        items = [
            ng.NounCreation(ng.Noun("Any", 10), ng.UnrecognizedLine([
                "other ",
                ng.Noun("text_node", 24),
                " here"
            ], 18), 10),
            ng.PrecedentCreation(ng.Precedent("precedent", 41), [
                ng.NounCreation(ng.Noun("new prec-noun", 53), ng.UnrecognizedLine([
                    "is a simple ",
                    ng.Noun("test", 82),
                    " with some ",
                    ng.Precedent("precedents", 99),
                    " for a while!"
                ], 70), 53)
            ], 41)
        ]
        expected.content = items
        self.parser_test(code, parser, expected)

    def test_comments(self):
        code = "// this is <comment> string that /may/ [contain] any characters "
        parser = ng.comment()
        expected = ng.Comment(" this is <comment> string that /may/ [contain] any characters ",0)
        self.parser_test(code, parser, expected)

        code = "// this is <comment> string that /may/ [contain] any characters\n"
        parser = ng.comment()
        expected = ng.Comment(" this is <comment> string that /may/ [contain] any characters",0)
        self.parser_test(code, parser, expected)

    def test_iconix_entry(self):
        code = "// this is <comment> string that /may/ [contain] any characters "
        parser = ng.iconix_entry()
        expected = ASTTree([ng.Comment(" this is <comment> string that /may/ [contain] any characters ",0)])
        self.parser_test(code, parser, expected)