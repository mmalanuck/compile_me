# -*- coding: utf8 -*-
__author__ = 'cjay'

from compile_me import lexer

RESERVED = 'RESERVED'
ENDLINE = "ENDLINE"
# ENDFILE = "ENDFILE"
STARTLINE = "STARTLINE"
SPACE = "SPACE"
TAB = "TAB"
COMMENT = "COMMENT"
TEXT = 'TEXT'

token_exprs = [
    (r'/', RESERVED),
    (r'\t', TAB),
    (" ", SPACE),
    (r'-', RESERVED),
    (r'-', RESERVED),
    (r'<', RESERVED),
    (r'>', RESERVED),
    (r'\[', RESERVED),
    (r'\]', RESERVED),
    (r'[^><\[\]\t\n/-]+', TEXT),
    (r'\n', ENDLINE)
]


def trim_white_space(txt):
    start = 0
    end = len(txt)
    while txt[start] == " ":
        start += 1
        if start == end:
            break
    while txt[end-1] == " ":
        end -= 1
        if end <= start:
            break
    return txt[start:end]


def trim_white_space_processor(token, tokens, pos):
    """
    выбрасывает все пустые (или с пробелами) текстовые токены в постобработке
    """
    if token[1] == TEXT:
        trimmed = trim_white_space(token[0])
        if not trimmed:
            return None
        else:
            return (trimmed, TEXT, tokens[pos][2])
    else:
        return token

def modify_comments(token, tokens, pos):
    """
    Добавляет токен конца строки после коммента
    :param token:
    :param tokens:
    :param pos:
    :return:
    """
    if token and token[1] == COMMENT:
        return [token, ("\n", ENDLINE, pos)]
    return token


def dummy_pocessor(token, tokens, pos):
    return token

def add_newline_token(token, tokens, pos):
    "Добавляет токен новой строки после токена конца строки"
    if token and token[1] == ENDLINE:
        return [token, ("", STARTLINE, tokens[pos][2]+1)]
    return token

def remove_many_newlines(token, tokens, pos):
    if token and token[1] == ENDLINE:
        if pos > 0:
            if tokens[pos-1][1] == ENDLINE:
                return None

    return token

def post_process(tokens):
    '''
    Постобработка токенов
    '''

    # подключенные процессоры, модифицирующие список токенов
    # processors = [trim_white_space_processor,
    #               remove_many_newlines,
    #               add_newline_token]

    processors = []


    # res = [("", STARTLINE, 0)]
    res = []
    item = None
    for pos, token in enumerate(tokens):
        item = token
        for i in processors:
            item = i(item, tokens, pos)
            if not item:
                continue
        if not item:
            continue
        if isinstance(item, list):
            res.extend(item)
        else:
            res.append(item)
    # if len(tokens) > 1:
    #     res.append(("", ENDFILE, tokens[-1][2]))
    # else:
    #     res.append(("", ENDFILE, 0))
    return res

def lex(characters):
    tokens = lexer.lex_me(characters, token_exprs)
    return post_process(tokens)